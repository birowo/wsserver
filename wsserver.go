package wsserver

import (
	"crypto/sha1"
	"encoding/base64"
)

const secWebsocketKey = "012345678911234567892123"
const resHdrs = "HTTP/1.1 101 Switching Protocols\r\n" +
	"Upgrade: websocket\r\n" +
	"Connection: Upgrade\r\n" +
	"Sec-WebSocket-Accept: "
const bgn = len(resHdrs)
const bgn_ = bgn + len(secWebsocketKey)
const mgcStr = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
const end = bgn_ + len(mgcStr)
const rnrn = "\r\n\r\n"

var end_ = bgn + base64.StdEncoding.EncodedLen(20 /*sha1 result size*/)
var End = end_ + len(rnrn)

func Upgrade(sWsKey string, bfr []byte) {
	copy(bfr, resHdrs)
	copy(bfr[bgn:], sWsKey)
	copy(bfr[bgn_:], mgcStr)
	hash := sha1.Sum(bfr[bgn:end])
	base64.StdEncoding.Encode(bfr[bgn:], hash[:])
	copy(bfr[end_:], rnrn)
}

func unmask(payload []byte, len_ int, maskKey []byte) {
	for i := 0; i < len_; i++ {
		payload[i] ^= maskKey[i&3]
	}
}

const (
	OpCont  = 0x00
	OpTxt   = 0x01
	OpBin   = 0x02
	OpClose = 0x08
	OpPing  = 0x09
	OpPong  = 0x0a
)

func ParseFrame(bfr []byte) (fin bool, opCode byte, bgn, len_ int) {
	// payload form client to server should be masked
	if (bfr[1] & 0x80) != 0x80 { //if not masked
		return
	}
	fin = (bfr[0] & 0x80) == 0x80
	opCode = bfr[0] & 0x0f
	len_ = int(bfr[1]) & 0x7f
	switch len_ {
	case 0:
		bgn = 2
	case 126:
		bgn = 4
		len_ = ((int(bfr[2]) << 8) | int(bfr[3]))
		unmask(bfr[8:8+len_], len_, bfr[bgn:8])
	case 127:
		return //not implemented yet
	default:
		bgn = 2
		unmask(bfr[6:6+len_], len_, bfr[bgn:6])
	}
	return
}
func Pong(bfr []byte, end int) {
	bfr[0] ^= 0b11 //pong <- ping
	bfr[1] &= 0x7f
	copy(bfr[2:], bfr[6:end+4])
}
func Replay(bfr []byte, bgn, end int) {
	bfr[1] &= 0x7f //clear (unmask)
	copy(bfr[bgn:], bfr[bgn+4:end+4])
}
